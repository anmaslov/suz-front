export default {
  appName: 'СУЗ',
  appVersion: '{{ variable.version }}',
  appCommit: '{{ variable.commit }}',
  menu: [
    {title: "Заявки", to: '/', icon: 'clear_all', inToolbar: false},
    {title: "Акты", to: '/reports', icon: 'flag', inToolbar: true},
    {title: "Клиенты", to: '/customers', icon: 'face', inToolbar: false},
    {title: "Пользователи", to: '/users', icon: 'group', inToolbar: false},
  ]
}
