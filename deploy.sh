#!/bin/bash

DIR="/home/deploy/suz_front"
DIR_FROM="/home/deploy/tmp"
DEST="$DIR/deploy_$1"

rm -rf $DEST
mkdir $DEST -p
#cp $DIR_FROM/docker-compose-production.yml $DEST/docker-compose.yml
sed "s/{image_tag}/$1/" $DIR_FROM/.env > $DEST/.env
sed "s/suz-front:latest/suz-front:$1/" $DIR_FROM/docker-compose-production.yml > $DEST/docker-compose.yml
cd $DEST
docker-compose pull
docker-compose up --build --remove-orphans -d
