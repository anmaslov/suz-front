FROM node:12-alpine as build
ENV APP_ROOT /web
ENV NODE_ENV production
ENV BASE_URL "https://api.inntech29.ru"
ENV SOCKET_URL "wss://api.inntech29.ru"
WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}
RUN yarn --production
RUN yarn build

######################

FROM nginx:1.13.12-alpine as production
COPY --from=build /web/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

#FROM node:alpine
#ENV NODE_ENV="production"
#COPY --from=build /web/dist /var/node
#WORKDIR /var/node
#EXPOSE 3000
#ENTRYPOINT ["./bin/www"]

#docker build -t suz-front:v1 . --rm
